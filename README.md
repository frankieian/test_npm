# test_npm

This package is an example of how to create a NPM package using the gitlab registry.

# How to update package?

To update package you need to update the package.json with a new version number. Once that is pushed to gitlab, a job will be created that will deploy the package. You can update a package manually by doing the same but with command `npm publish` in your local machine. The local machine must have configured authorisation to upload to registry.

# How to install the package?

To install the package, you need to implement a few configuration changes to NPM for your project. The commands needed to be run in terminal in your project are:

`npm config set @frankieian:registry https://gitlab.com/api/v4/packages/npm/`

For private packages authentication needs to be provided by using this command:

`npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "<your_token>"`

You can replace <your_token> with a token obtained from gitlab, either personal access token or deploy token (from owner of repository)

More information can be found in the link provided below

Complete Guide:
https://docs.gitlab.com/ee/user/packages/npm_registry/index.html#enabling-the-npm-registry