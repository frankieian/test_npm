class ApiError extends Error{
    constructor(code, status, title, details){
        super();
        this.code = code;
        this.status = status;
        this.tile = title;
        this.details = details;

    }
}

class ExternalError extends ApiError{
    constructor(code, status, title, details, source){
        super(code, status, title, details)
        this.solutions = "External Error, 3rd party APIs offline";
        this.source = source;
    }
}

class UserNotFound extends ApiError{
    constructor(code, status, title, details, source){
        super(code, status, title, details)
        this.solutions = "Please enter correct details!"
        this.source = source
    }
}

module.exports = {ApiError, ExternalError, UserNotFound}

